"""
Checking the node degree distribution in epsilon networks of different
sample distributions. Using low-dimensional samples for interpretability
(results could differ in dimensions >= number of samples, I don't know).

@author: frbourassa
June 15, 2022
"""

import numpy as np
import scipy as sp
from scipy.spatial import cKDTree
import matplotlib.pyplot as plt

def test_samples(samples, epsils):
    # Compute number of neighbours within radius epsilon of each point
    tree = cKDTree(data=samples)
    n_neighbors_list = []
    for ep in epsils:
        n_neighbors_each = tree.query_ball_point(samples, r=ep,
                                            n_jobs=2, return_length=True)
        n_neighbors_list.append(n_neighbors_each)

    # Show what the samples look like and show the density of neighbours.
    n_cols = 2
    n_rows = (len(epsils) + 1) // n_cols + min(1, (len(epsils)+1) % n_cols)
    fig = plt.figure()
    gs = fig.add_gridspec(n_rows, n_cols)
    fig.set_size_inches(3.25*2, 3.0*n_rows)

    ax = fig.add_subplot(gs[0, 0])
    skp = samples.shape[0] // 8000
    ax.scatter(samples[::skp, 0], samples[::skp, 1], s=0.5, alpha=0.6, color="k")
    ax.set(xlabel=r"$x_1$", ylabel=r"$x_2$", aspect="equal")
    axes = [ax]

    ax0 = fig.add_subplot(gs[0, 1])
    axes.append(ax0)
    for i in range(len(epsils)):
        row, col = (i+1) // n_cols, (i+1) % n_cols
        if i == 0:
            ax = ax0
        else:
            ax = fig.add_subplot(gs[row, col], sharey=ax0, sharex=ax0)
            axes.append(ax)

        # Compute distribution of number of neighbours, log scale of x axis.
        n_neighbors_each = n_neighbors_list[i]
        binseps = np.histogram_bin_edges(np.log(n_neighbors_each), bins="doane")
        #binseps = np.unique(np.exp(binseps).astype(int))
        binseps = np.exp(binseps)
        counts, binseps = np.histogram(n_neighbors_each, bins=binseps)
        binwidths = np.diff(binseps)  # For plotting

        # Count total probability of discrete values in a bin,
        # and divide by the number of integers that fit in each bin,
        # to have the average probability per k value in the bin.
        # This seems like the best way to see scaling laws in the underlying
        # discrete distribution p(k).
        int_bin_width, _ = np.histogram(np.arange(binseps[0], binseps[-1]+1), bins=binseps)
        # Avoid divisions by zero in bins that will be empty
        int_bin_width = int_bin_width.clip(1, np.inf)
        pdf = counts / n_neighbors_each.size / int_bin_width

        ax.bar(x=binseps[:-1], height=pdf, width=binwidths, align="edge",
                facecolor="w", edgecolor="k")
        ax.set(xlabel="Node degree", ylabel="Probability density",
            yscale="log", xscale="log", alpha=0.7)
        ax.set_title(r"$\epsilon = {}$".format(epsils[i]))

    fig.tight_layout()
    return fig, axes


if __name__ == "__main__":
    # Generate quite a few 2d gaussian(0, 1) samples
    # Not too many, otherwise this will be slow.
    n_samp = int(5e4)
    n_dims = 2
    rgen = np.random.default_rng(seed=0x1f3e047061ada79267fc8cf1f5a367aa)

    gauss_samples = rgen.standard_normal(size=(n_samp, n_dims))
    fig, axes = test_samples(gauss_samples, epsils=[0.02, 0.1, 0.5])
    #fig.savefig("figures/neighbor_density_from_gaussian.pdf", transparent=True)
    plt.show()
    plt.close()

    # Now test uniform samples
    unif_samples = rgen.random(size=(n_samp, n_dims))
    fig, axes = test_samples(unif_samples, epsils=[0.005, 0.02, 0.1])
    #fig.savefig("figures/neighbor_density_from_uniform.pdf", transparent=True)
    plt.show()
    plt.close()

    # Multimodal data: gaussian mixture
    n_dims = 2
    n_samp = int(4e4)
    scales = np.asarray([[1.0, 1.0], [1.0, 1.0], [0.5, 2.], [2., 1.0]])
    means = np.asarray([[0.0, 0.0], [3.0, 2.0], [-2.5, -3.5], [-1.5, 5.0]])
    gaussmix_samples = []
    for i in range(4):
        gaussi = rgen.standard_normal(size=(n_samp//4, n_dims))
        gaussi *= scales[i:i+1]
        gaussi += means[i:i+1]
        gaussmix_samples.append(gaussi)
    del gaussi
    gaussmix_samples = np.concatenate(gaussmix_samples, axis=0)
    fig, axes = test_samples(gaussmix_samples, epsils=[0.05, 0.1, 2.5])
    #fig.savefig("figures/neighbor_density_from_gaussmix.pdf", transparent=True)
    plt.show()
    plt.close()

    # Gaussian in high dimension
    # Warning: this part takes a couple minutes to run.
    n_dims = 10
    gauss_samples_hd = rgen.standard_normal(size=(n_samp, n_dims))

    fig, axes = test_samples(gauss_samples_hd, epsils=[1.5, 2.5, 3.75])
    #fig.savefig("figures/neighbor_density_from_gaussian_highdim.pdf", transparent=True)
    plt.show()
    plt.close()
