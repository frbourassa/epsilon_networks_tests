# Testing cluster detection and density in 𝜀 networks

## Authors
François X. P. Bourassa and Paul François.  Department of Physics, McGill University, Montréal, Québec, Canada.

## Requirements
This Python code was tested on Mac (macOS Catalina 10.15.7) and Linux (Linux 3.2.84-amd64-sata x86_64) with the Anaconda 2020.07 distribution, with the following versions for important packages:
 - Python 3.7.6
 - numpy 1.19.2
 - scipy 1.5.2
 - matplotlib 3.3.2
 - scikit-learn 0.23.2
 - networkx 2.5
