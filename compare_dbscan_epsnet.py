""" Script checking whether the connected components found with the epsilon
network method (implemented using pairwise distance matrix and networkx)
and the DBSCAN algorithm with minPts=2 are identical or not.

Test case: two well-separated gaussian distributions, using a distance
a lot smaller than inter-distribution distance but small enough to ensure
multiple small connected components in each gaussian distribution.

@author: frbourassa
June 16, 2022
"""

import numpy as np
from sklearn.cluster import DBSCAN
import networkx as nx
from scipy.spatial.distance import pdist, squareform

import matplotlib.pyplot as plt

def find_cc_dbscan(samples, epsil):
    """ Return a set of connected components, each component being a
    set of the indices of samples belonging to that cluster.
    """
    n_jobs = 2
    labels = DBSCAN(epsil, min_samples=2, n_jobs=n_jobs).fit_predict(samples)
    # Reject noise samples not in clusters (those with label -1)
    label_names = np.unique(labels)
    label_names = label_names[1:]

    # Return a set of sample indices belonging to each connected component
    connected_comps = [set(np.nonzero(labels == l)[0]) for l in label_names]
    connected_comps = connected_comps
    return connected_comps

def find_cc_networkx(samples, epsil):
    """ Return a set of connected components, each component being a
    set of the indices of samples belonging to that cluster.
    Don't use on more than n=1e4 samples, because memory use is n^2.
    """
    # Build the adjacency matrix
    adjacency_mat = squareform((pdist(samples) <= epsil).astype(bool))
    # Convert to a graph with networkx
    graph = nx.Graph(adjacency_mat)

    # Find connected components with networkx
    connected_comps = nx.connected_components(graph)
    # Only return components larger than 1
    return [a for a in connected_comps if len(a) > 1]


if __name__ == "__main__":
    rgen = np.random.default_rng(seed=0xbab59badf523d05e6fa70a723251b993)
    ndim = 2
    nsamp = 4000
    comp_size = (nsamp, ndim)
    epsilon = 0.05
    samples = np.vstack([rgen.standard_normal(size=comp_size),
            rgen.standard_normal(size=comp_size) + 4.0])

    connected_comps_nx = find_cc_networkx(samples, epsilon)
    msg = "Found {} connected components with "
    print(msg.format(len(connected_comps_nx)) + "networkx")

    connected_comps_dbscan = find_cc_dbscan(samples, epsilon)
    print(msg.format(len(connected_comps_dbscan)) + "DBSCAN")

    if len(connected_comps_nx) != len(connected_comps_dbscan):
        raise AssertionError("Different number of components found by DBSCAN and networkx")

    # Compare sets
    for a in connected_comps_dbscan:
        if a not in connected_comps_nx:
            raise AssertionError("Component {} found by DBSCAN not by networkx".format(a))
    print("Identical connected components found with both algorithms")
