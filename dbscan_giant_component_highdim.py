"""
The epsilon network construction method is just the DBSCAN clustering
algorithm with minPts set to 2: any point with a neighbor can form
a connected component counted by the algorithm.
Set to 2, not 1, to discard all lonely points.

Here, testing on samples from a relatively high-dimensional (20D) space where
clusters are distinguished along a few axes only, while they have identical
marginal distributions along other axes.

20D is still a small dimension compared to scRNAseq data (10k-100k genes):
expect the effect to be even more pronounced in that case.

WARNING: the "linux" case runs in > 1 hour on an 8-core (old) CPU.
Change the conditions on sys_platform depending on your use case.

@author: frbourassa
June 15, 2022
"""
import numpy as np
from sklearn.cluster import DBSCAN

import matplotlib as mpl
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from sys import platform as sys_platform

# On the Physics cluster, can't show plots
if sys_platform.startswith("linux"):
    mpl.use("Agg")

from dbscan_giant_component import giant_comp_size_vs_epsilon


# Plot a 3d scatter plot to show
def plot_gcc_vs_epsilon_3d(samples, epsil_range, gcc_sizes):
    fig = plt.figure()
    gs = fig.add_gridspec(1, 2)
    fig.set_size_inches(3.5*2, 3.5)

    ax = fig.add_subplot(gs[0, 0], projection="3d")
    skp = samples.shape[0] //  min(8000, samples.shape[0])
    ax.scatter(samples[::skp, 0], samples[::skp, 1], samples[::skp, 2],
                s=0.5, alpha=0.6, color="k")
    ax.view_init(elev=70, azim=4)
    ax.set(xlabel=r"$x_1$", ylabel=r"$x_2$", zlabel=r"$x_3$")

    ax2 = fig.add_subplot(gs[0, 1])
    ax2.plot(epsil_range, gcc_sizes, color="k")
    ax2.set(xlabel=r"Radius $\epsilon$", ylabel="Giant CC size")
    fig.tight_layout()
    axes = [ax, ax2]
    return fig, axes


if __name__ == "__main__":
    # Multimodal data: gaussian mixture
    n_dims = 20
    n_samp = int(4e4)
    rgen = np.random.default_rng(seed=0xcfe882ea364016c7ea9a0bb0a82e5724)

    # 20 dimensions, two are different, all others are similar
    scales = np.ones([4, n_dims])
    scales[:, 0:2] = np.asarray([[1.0, 1.0], [1.0, 1.0], [0.5, 2.], [2., 1.0]])
    means = np.zeros([4, n_dims])
    means[:, 0:2] = np.asarray([[0.0, 0.0], [3.0, 2.0], [-2.5, -3.5], [-1.5, 5.0]])
    gaussmix_samples = []
    for i in range(4):
        gaussi = rgen.standard_normal(size=(n_samp//4, n_dims))
        gaussi *= scales[i:i+1]
        gaussi += means[i:i+1]
        gaussmix_samples.append(gaussi)
    del gaussi
    gaussmix_samples = np.concatenate(gaussmix_samples, axis=0)

    # Choose a relevant range of epsilons (this required some fiddling)
    if sys_platform.startswith("linux"):
        # Long calculation
        epsilon_range = np.linspace(2.0, 5.0, 201)
    elif sys_platform.startswith("darwin"):
        # Test case
        epsilon_range = np.arange(2.0, 5.0, 1.0)

    giant_comp_sizes = giant_comp_size_vs_epsilon(gaussmix_samples, epsilon_range)
    #giant_comp_sizes = np.load("output/giant_component_sizes_highdim.npy")
    print(giant_comp_sizes)

    # Write to disk to avoid re-running in case the plotting crashes
    np.save("output/giant_component_sizes_highdim.npy", giant_comp_sizes)

    # Plot the results
    fig, ax = plot_gcc_vs_epsilon_3d(gaussmix_samples, epsilon_range, giant_comp_sizes)

    if sys_platform.startswith("darwin"):
        plt.show()
        plt.close()
    elif sys_platform.startswith("linux"):
        fig.savefig("figures/gaussmix_gcc_size_vs_epsilon_highdim.pdf", transparent=True)
