"""
The epsilon network construction method is just the DBSCAN clustering
algorithm with minPts set to 2: any point with a neighbor can form
a connected component counted by the algorithm.
Set to 2, not 1, to discard all lonely points.

@author: frbourassa
June 15, 2022
"""
import numpy as np
from sklearn.cluster import DBSCAN

import matplotlib as mpl
import matplotlib.pyplot as plt
from sys import platform as sys_platform

# On the Physics cluster, can't show plots
if sys_platform.startswith("linux"):
    mpl.use("Agg")


# Version 1: for every epsilon, take largest component, even
# if it's not the same component from one epsilon to the next.
# Easiest to implement and should have step-like behaviour anyways
# when large, separate clusters fuse.


def giant_comp_size_vs_epsilon(samples, epsil_range):
    gcomp_sizes = []
    if sys_platform.startswith("darwin"):
        n_jobs = 2
    elif sys_platform.startswith("linux"):
        n_jobs = -1
    else:
        n_jobs = 1
    for e in epsil_range:
        labels = DBSCAN(e, min_samples=2, n_jobs=n_jobs).fit_predict(samples)
        # Reject noise samples not in clusters (those with label -1)
        labels = labels[labels != -1]
        # Find the size of the largest cluster
        clust_names, clust_counts = np.unique(labels, return_counts=True)
        if clust_counts.size > 0:
            gcomp_sizes.append(np.max(clust_counts))
        else:
            gcomp_sizes.append(1)
        print("Completed epsilon = ", e)
    return np.asarray(gcomp_sizes)


def plot_gcc_vs_epsilon(samples, epsil_range, gcc_sizes):
    fig, axes = plt.subplots(1, 2)
    fig.set_size_inches(3.5*2, 3.5)
    ax = axes[0]
    skp = samples.shape[0] //  min(8000, samples.shape[0])
    ax.scatter(samples[::skp, 0], samples[::skp, 1], s=0.5, alpha=0.6, color="k")
    ax.set(xlabel=r"$x_1$", ylabel=r"$x_2$", aspect="equal")

    ax = axes[1]
    ax.plot(epsil_range, gcc_sizes, color="k")
    ax.set(xlabel=r"Radius $\epsilon$", ylabel="Giant CC size")
    fig.tight_layout()
    return fig, axes



if __name__ == "__main__":
    # Multimodal data: gaussian mixture
    n_dims = 2
    n_samp = int(4e4)
    rgen = np.random.default_rng(seed=0xcfe882ea364016c7ea9a0bb0a82e5724)

    scales = np.asarray([[1.0, 1.0], [1.0, 1.0], [0.5, 2.], [2., 1.0]])
    means = np.asarray([[0.0, 0.0], [3.0, 2.0], [-2.5, -3.5], [-1.5, 5.0]])
    gaussmix_samples = []
    for i in range(4):
        gaussi = rgen.standard_normal(size=(n_samp//4, n_dims))
        gaussi *= scales[i:i+1]
        gaussi += means[i:i+1]
        gaussmix_samples.append(gaussi)
    del gaussi
    gaussmix_samples = np.concatenate(gaussmix_samples, axis=0)

    # Choose a relevant range of epsilons (this required some fiddling)
    if sys_platform.startswith("linux"):
        # Long calculation
        epsilon_range = np.linspace(0.01, 0.2, 201)
    elif sys_platform.startswith("darwin"):
        # Test case
        epsilon_range = np.arange(0.01, 0.2, 0.05)

    giant_comp_sizes = giant_comp_size_vs_epsilon(gaussmix_samples, epsilon_range)
    print(giant_comp_sizes)

    fig, ax = plot_gcc_vs_epsilon(gaussmix_samples, epsilon_range, giant_comp_sizes)

    if sys_platform.startswith("darwin"):
        plt.show()
        plt.close()
    elif sys_platform.startswith("linux"):
        fig.savefig("figures/gaussmix_gcc_size_vs_epsilon.pdf", transparent=True)
